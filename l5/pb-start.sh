#!/bin/bash
ansible-galaxy install -r roles_requirements.yml
ansible-playbook -vvvv \
		--vault-id app-server-dev@~/.app-server-dev-vault \
		-i inventories/dev/hosts \
		--skip-tags "" \
                 pb.yaml
