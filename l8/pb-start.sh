#!/bin/bash
ansible-galaxy collection install -r requirements.yml
ansible-playbook -vv -i inventories/dev/hosts --skip-tags "" pb.yaml
