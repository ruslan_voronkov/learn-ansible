import os
import testinfra.utils.ansible_runner
import urllib.request

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
        os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

def test_urlopen(host):
    def_ip = host.interface(getattr(host.interface.default(), 'name'), "inet").addresses[0]
    assert urllib.request.urlopen("http://" + def_ip).getcode() == 200

