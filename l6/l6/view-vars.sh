#!/bin/bash
ansible-inventory -vvvv \
                --vault-id app-server-dev@~/.app-server-dev-vault \
                -i inventories/dev/ \
               --graph --vars 

