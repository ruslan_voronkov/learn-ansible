#!/bin/bash
ansible-playbook -vvvv \
		--vault-id app-server-dev@~/.app-server-dev-vault \
		-i inventories/dev/hosts \
		--extra-vars "ansible_user=root ansible_port=21757" \
		--skip-tags "" \
                 pb.yaml
